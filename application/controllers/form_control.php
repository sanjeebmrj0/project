<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_control extends CI_controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->library('form_validation');
	}
	function index()
	{
			
		if($_POST)
		{
			$this->form_validation->set_rules('name','Name','required');
			$this->form_validation->set_rules('phone','Phone','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('address','Address','required');
			$this->form_validation->set_rules('nationality','Nationality','required');
			$this->form_validation->set_rules('defaultsettings_birthDay','Date of Birth','required');
			$this->form_validation->set_rules('edu','Education','required');
			
			if($this->form_validation->run() == TRUE)
			{
				$this->load->model('form_model');
				if($this->form_model->insert_data())
				{
					$this->load->view('form_data_view');
				}
				else
				{
					$this->session->set_flashdata('error','Error in submitting form');
				}
			}
			
		}
		else
		{

			$this->load->view('form_view');
		}
	}

}