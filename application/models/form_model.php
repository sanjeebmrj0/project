<?php
class Form_model extends CI_model
{
	function insert_data()
	{
		$data = array('name' => $_POST['name'],
					'gender' => $_POST['gender'],
					'phone' => $_POST['phone'],
					'email' => $_POST['email'],
					'address' => $_POST['address'],
					'nationality' => $_POST['nationality'],
					'dob' => $_POST['defaultsettings_birthDay'],
					'edu' => $_POST['edu'],
					'mode_to_contact' => $_POST['mode_to_contact'] );

		if($this->db->insert('form',$data))
		{
			$query = $this->db->get('form');
			$result = $query->result_array();
			$file = fopen('form_data.csv', 'w');
			foreach ($result as $row)
			{
				$csvData = $row['id'].','.$row['name'].','.$row['gender'].','.$row['email'].','.$row['address'].','.$row['nationality'].','.$row['dob'].','.$row['edu'].','.$row['mode_to_contact']."\r\n";
			    fputcsv($file, array($csvData));
			}
			 
			fclose($file);
			return true;
		}
		else
		{
			return false;
		}
	}
}