<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Table View</h2>
                                                                                       
  <div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Address</th>
        <th>Nationality</th>
        <th>Date of Birth</th>
        <th>Education</th>
        <th>Mode to Contact</th>
      </tr>
    </thead>
    <tbody>
    <!-- Fetched CSV File  -->
    <?php
    $CSVfp = fopen("form_data.csv", "r");

          if($CSVfp !== FALSE) 
          {
            while(! feof($CSVfp)) 
            {
                $data1 = fgetcsv($CSVfp, 1000, ",");
                if(is_array($data1))
                {
                  foreach($data1 as $key1) 
                  {
        
                    $data2 = explode(',', $key1);
                    echo  '<tr>';
                    foreach ($data2 as $key2) 
                    {
                    ?>
                      <td><?=$key2; ?></td>
                      <?php
                    }
                    echo '</tr>';
                  }
  
                }
              }
          }
          fclose($CSVfp);?>
    </tbody>
  </table>
  </div>
</div>

</body>
</html>
