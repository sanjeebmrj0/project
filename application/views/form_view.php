<!DOCTYPE html>
<html>
<head>
    <title>Form</title>
    <!-- js and CSS links for this form -->
    <link rel="stylesheet" href="<?= site_url('assets');?>/vendor/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="<?= site_url('assets');?>/dist/css/formValidation.css"/>
    <script type="text/javascript" src="<?= site_url('assets');?>/vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets');?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets');?>/dist/js/formValidation.js"></script>
    <script src="<?= site_url('assets');?>/js/jquery-birthday-picker.min.js"></script>
    <script type="text/javascript" src="<?= site_url('assets');?>/dist/js/framework/bootstrap.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="page-header">
                    <h2>Fill The form</h2>
                </div>
                <!-- Message for success or error in form submittion  -->
                <?php if($this->session->flashdata('success')):?>
                <h6><?= $this->session->flashdata('success');?></h6>
                <?php else: ?>
                <h6><?= $this->session->flashdata('error');?></h6>
                <?php endif;?>
                <form id="defaultForm" method="post" class="form-horizontal" action="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Full name</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="name" placeholder="Full name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Gender</label>
                        <div class="col-sm-6">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="male" /> Male
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="female" /> Female
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="gender" value="other" /> Other
                                </label>
                            </div>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="phone" placeholder="phone number" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="email" placeholder="Valid Email ID" />
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="address" placeholder="Recent Address"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nationality</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="nationality" />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Date Of Birth</label>
                        <div class="col-sm-5">
                            <div id="default-settings" class="form-control"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Education Background</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="edu" />
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Preferred Mode To Contact</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="mode_to_contact" >
                                <option value="" disabled selected>Select mode to Contact you</option>
                                <option value="email">Email</option>
                                <option value="phone">Phone</option>
                            </select>
                        </div>
                    </div>
                    <div id="default-settings"></div>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary" name="sumbitt" value="Sign up">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Required js for form Validation -->
<script type="text/javascript">
$(document).ready(function() {
    // Generate a simple captcha
    // function randomNumber(min, max) {
    //     return Math.floor(Math.random() * (max - min + 1) + min);
    // };
    // $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200), '='].join(' '));

    $('#defaultForm').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstName: {
                row: '.col-sm-4',
                validators: {
                    notEmpty: {
                        message: 'The full name is required'
                    }
                }
            },
            address: {
                row: '.col-sm-5',
                validators: {
                    notEmpty: {
                        message: 'The Address is required'
                    }
                }
            },
            edu: {
                row: '.col-sm-5',
                validators: {
                    notEmpty: {
                        message: 'The Education Background is required'
                    }
                }
            },
            nationality: {
                row: '.col-sm-5',
                validators: {
                    notEmpty: {
                        message: 'The Nationality is required'
                    }
                }
            },
            phone: {
                row: '.col-sm-4',
                validators: {
                    notEmpty: {
                        message: 'The phone number is required'
                    }
                }
            },
             defaultsettings_birthDay: {
                row: '.col-sm-5s',
                validators: {
                    notEmpty: {
                        message: 'The Date of Birth is required' 
                    }
                }
            },
            mode_to_contact: {
                row: '.col-sm-5',
                validators: {
                    notEmpty: {
                        message: 'Select Mode to Contact You'
                    }
                }
            },
            username: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'The username is required'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The username must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'The username can only consist of alphabetical, number, dot and underscore'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    },
                    different: {
                        field: 'username',
                        message: 'The password cannot be the same as username'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'The gender is required'
                    }
                }
            },
            captcha: {
                validators: {
                    callback: {
                        message: 'Wrong answer',
                        callback: function(value, validator, $field) {
                            var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                            return value == sum;
                        }
                    }
                }
            },
            agree: {
                validators: {
                    notEmpty: {
                        message: 'You must agree with the terms and conditions'
                    }
                }
            }
        }
    });
});
</script>
<script>
$("#default-settings").birthdayPicker();    
                $("#default-birthday").birthdayPicker({"defaultDate":"01-03-1980"});
                $("#max-year-birthday").birthdayPicker({
                    "defaultDate": "01-03-1980",
                    "maxYear": "2020",
                    "maxAge": 65
                });
                $("#short-month-birthday").birthdayPicker({
                    "defaultDate": "01-03-1980",
                    "maxYear": "2020",
                    "maxAge": 65,
                    "monthFormat":"short"
                });
                $("#long-month-birthday").birthdayPicker({
                    "defaultDate": "01-03-1980",
                    "maxYear": "2020",
                    "maxAge": 65,
                    "monthFormat":"long",
                    "sizeClass": "span3"
                });         
</script>
</body>
</html>