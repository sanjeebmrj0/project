Client section Form
====================

This is client section form where client can fill their detail
##Dependencies

##Porject Detail
1. This project is developed on CodeIgniter 2 using Bootstrap Design Framework
2. Mysql database and form_data.csv file are used to store data from form.
3. Required database file and form_data.csv file ard stored in root directory.
3. All the required JS and CSS are stored in assets folder in root directory.


##Requirement
PHP 5.6.12
Apache 2.4.9
MySQL 5.6.17

##RUN Process
1. Install XAMPP/WAMP/MAMP
2. Copy project folder in www folder in WAMP OR htdoc in XAMPP folder
3. open http://localhost/phpmyadmin
4. create database project and import project.sql 
5. open http://localhost/project in any browser you use.

This form requires the following libraries:

1. The latest Jquery, which you can download from [here](http://jquery.com).
2. The latest Bootstrap, you can download from [here](http://getbootstrap.com/).

##Getting Started


link to open this form [here](http://localhost/project/).

##How to use Birthday Picker

Include the following css files into the header
```
<link href="http://example.com/css/bootstrap.min.css" rel="stylesheet">
```
Include the javascript files into the page
```
<script src="http://example.com/js/jquery.min.js"></script>

<script src="http://example.com/js/bootstrap.min.js"></script>

<script src="/dist/js/formValidation.js"></script>




##Form Submittion

The following are the initialization options and their default values
```
name : 

gender : 

maxYear : 

"phone" : ""

"email" : ""

"address" : 

"date of birth" : 

"education background"	: ""

"preferred Mode to conatct"	: ""
```

